#include <Arduino.h>
#include <SPI.h>

unsigned int duration = 500;
unsigned int frequency = 2;
bool state = false;

unsigned long prevMillis = 0;
unsigned long prevMillisDur = 0;

char buff[20];

volatile byte indx;
volatile boolean process;

void useData()
{
  char *freq = strchr(buff, 'f');
  char *dura = strchr(buff, 'd');

  if (freq != NULL || dura != NULL)
  {
    //int startF = freq - buff;
    //int startD = dura - buff;

    char *firstPart = strtok(buff, "d");

    char *remainingPart = strtok(NULL, "");

    if (firstPart)
    {
      firstPart = strtok(firstPart, "f");

      if (firstPart != 0)
      {
        if (freq != NULL)
        {
          frequency = atoi(firstPart);
          Serial.println("Frequency: " + (String)frequency);
        }
        else
        {
          duration = atoi(firstPart);
          Serial.println("Duration: " + (String)duration);
        }
      }
    }

    if (remainingPart)
    {
      {
        duration = atoi(remainingPart);
        Serial.println("Duration: " + (String)duration);
      }
    }
  }
  Serial.println();
}

void setup(void)
{
  Serial.begin(115200);
  pinMode(MISO, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  SPCR |= _BV(SPE); // turn on SPI in slave mode

  indx = 0;
  process = false;

  SPI.attachInterrupt();
}

ISR(SPI_STC_vect)
{
  char c = SPDR;

  if (indx < sizeof buff)
  {
    buff[indx++] = c;
    if (c == '\n')
      process = true;
  }
}

void loop(void)
{
  unsigned long currentMillis = millis();

  if (process)
  {
    Serial.print("SPI data received: ");
    for (int i = 0; i < indx; i++)
      Serial.print((char)buff[i]);
    useData();

    indx = 0;
    process = false;
  }
  else if (currentMillis - prevMillis >= 1000 / frequency)
  {
    state = !state;

    digitalWrite(LED_BUILTIN, state);

    prevMillis = currentMillis;
    prevMillisDur = currentMillis;
  }
  else if (state && (currentMillis - prevMillisDur >= duration))
  {
    digitalWrite(LED_BUILTIN, LOW);
    prevMillisDur = currentMillis;
  }
}