/*
 * Copyright (c) 2017 Linaro Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <console/console.h>

#include <device.h>
#include <drivers/spi.h>
#include <errno.h>

static uint8_t data[15];

static int stm32_spi_send(const struct device *spi,
						  const struct spi_config *spi_cfg,
						  const uint8_t *data, size_t len)
{
	const struct spi_buf_set tx = {
		.buffers = &(const struct spi_buf){
			.buf = (uint8_t *)data,
			.len = len,
		},
		.count = 1,
	};

	return spi_write(spi, spi_cfg, &tx);
}

static int stm32_spi_send_str(const struct device *spi,
							  const struct spi_config *spi_cfg,
							  const unsigned char *str)
{
	char end[2] = "\n";
	strcat(str, end);

	return stm32_spi_send(spi, spi_cfg, str, strlen(str));
}

void main(void)
{
	console_getline_init();

	const struct device *spi;
	struct spi_config spi_cfg = {0};
	int err;

	spi = device_get_binding("SPI_1");
	if (spi == NULL)
	{
		printk("Could not find SPI driver\n");
		return;
	}
	spi_cfg.operation = SPI_WORD_SET(8) | SPI_OP_MODE_MASTER;
	spi_cfg.frequency = 1000000U;

	printk("Enter a command to control SPI device!\n");
	printk("Format: f{frequency}(times per second), d{duration}(ms) Ex: f10d100\n");

	while (1)
	{
		char *s = console_getline();

		printk("Input line: %s\n", s);
		if (strchr(s, 'f') != NULL || strchr(s, 'd') != NULL)
		{
			err = stm32_spi_send_str(spi, &spi_cfg, s);
			if (err)
			{
				printk("Error writing to SPI! errro code (%d)\n", err);
				return;
			}
			else
				printk("SPI line sent: %s\n", s);
		}
		else
			printk("Wrong format!");
	}
}
